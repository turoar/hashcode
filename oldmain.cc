#include <iostream>
#include<string>
#include<fstream>
#include<vector>

using namespace std;

/**
 * Imprime el array en consola
 * 
 * */
void printArray(vector<int> array) {

    for(int i=0; i<array.size(); i++) {
        cout << "array ["<<i<<"]: "<<array[i]<<", ";
    }
    cout<<"\n";
}

/**
 * Obtener los valores del fichero de entrada
 * 
 * */
vector<int> getNumbersOfString(string line) {

    vector<int> array;
    string number = "";
    int slice = 0;

    for(int i=0; i<line.length(); i++) {
        if(line.at(i) == ' ') {
            array.push_back(slice);
            number = "";
        } else {
            number.push_back(line.at(i));
            slice = stoi(number);
            if(i==line.length()-1) {
                array.push_back(slice);
            }
        }
    }
    return array;
}

/**
 * Calcula la combinación de pizzas más cercano al valor de pizzas maximas
 * 
 * */
vector<int> calculatePizzas(int piezasMax, int tipos, vector<int> tamanyo) {
    
    int total=0;
    int piezas = 0;
    vector<int> tiposPizza, resultado;

    for(int i=0; i<tipos; i++) {        
        piezas=tamanyo[i];
        tiposPizza.push_back(i);
        for(int x=i+1; x<tamanyo.size(); x++) {
            if(piezas+tamanyo[x] <= piezasMax) {
                piezas = piezas +tamanyo[x];
                tiposPizza.push_back(x);
            }
        }
        for(int j=0; j<i; j++) {
            if(piezas+tamanyo[j] <= piezasMax) {
                piezas = piezas +tamanyo[j];
                tiposPizza.push_back(j);
            }
        }
        if((piezasMax-piezas) < (piezasMax-total)) {
            total=piezas;
            resultado=tiposPizza;
        }
        piezas=0;
        tiposPizza.clear();
    }
    return resultado;
}

/**
 * Imprime el array en un fichero
 * 
 * */
void escribirResultadoEnFichero(vector<int> array,string nombreFichero) {

    ofstream ficheroSalida;
    ficheroSalida.open(nombreFichero);
    ficheroSalida << array.size() << "\n";
    for(int i=0; i< array.size(); i++) {
        ficheroSalida << array.at(i);
        if(i<array.size()-1) {
            ficheroSalida << " ";
        }
    }
    ficheroSalida.close();
}

/**
 * Ordena el array de manera ascendente
 * 
 * */
vector<int> ordenarArrayAscendente(vector<int> array) {

    int aux;
    for(int i=0;i<array.size();i++){
        for(int x=i+1;x<array.size();x++){
            if(array[i]>array[x]){
                aux=array[i];
                array[i]=array[x];
                array[x]=aux;
            }
        }
    }
    return array;
}

int  main() {
   
   string line;
   char number;
   ifstream file ("e_also_big.in");
   vector<int> array, total;
   int piezas, tipos, tamanyo = 0;

   if(file.is_open()) {
        getline(file, line);
        array = getNumbersOfString(line);
        piezas = array[0];
        tipos = array[1];

        getline(file, line);
        array = getNumbersOfString(line);
        total = ordenarArrayAscendente(calculatePizzas(piezas, tipos, array));
       
      file.close(); 
   }

   escribirResultadoEnFichero(total,"e_also_big.txt");
}