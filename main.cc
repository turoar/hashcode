#include <iostream>
#include <string>
#include <fstream>
#include <vector>

#include "book.h"
#include "library.h"

using namespace std;

void escribirResultadoEnFichero(vector<Library> libraries, string nombreFichero)
{

    ofstream ficheroSalida;
    ficheroSalida.open(nombreFichero);
    ficheroSalida << libraries.size() << "\n";
    for (int i = libraries.size()-1; i >= 0; i--)
    {
        ficheroSalida << libraries[i].getID() << " ";
        ficheroSalida << libraries[i].getBook().size() << "\n";
        for (int j = 0; j < libraries[i].getBook().size(); j++)
        {
            ficheroSalida << libraries[i].getBook()[j].getID();
            if (j < libraries[i].getBook().size() - 1)
            {
                ficheroSalida << " ";
            }
        }

        ficheroSalida << "\n";
    }
    ficheroSalida.close();
}

void meteMierda(vector<int> &seleccionados, vector<Book> libros){

    for(int i = 0; i < libros.size(); i++){
        bool find = false;
        int pos = 0;
        for(int j = 0; j < seleccionados.size() && !find; j++){
            if(seleccionados[i] == libros[j].getID()){
                find = true;
                pos = j;
            }
        }
        if(find)
            seleccionados.push_back(libros[pos].getID());
    }
}

int main()
{
    int days, books, n_librarys;
    ifstream fi("./input/a_example.txt");
    vector<int> scores;
    vector<Library> librarys;
    for(int i=0;i<librarys.size();i++)
        librarys[i].ordenarArrayAscendente();

    if (fi.is_open())
    {
        // Primera linea de lectura
        fi >> books;
        fi >> n_librarys;
        fi >> days;
        // Cargamos las puntuaciones
        for(int i = 0; i < books; i++){
            int aux;
            fi >> aux;
            scores.push_back(aux);
        }
        for(int i = 0; i < n_librarys; i++){
            int n_books, n_signup, n_books_per_day;
            Library library;
            
            fi >> n_books; fi >> n_signup; fi >> n_books_per_day;
            library.setID(i);
            library.setTimeToScan(n_signup);
            library.setScanPerDay(n_books_per_day);
            vector<Book> library_books;

            for(int j = 0; j < n_books; j++){
                int aux; fi >> aux;
                Book bookie = Book(aux,scores[aux]);
                library_books.push_back(bookie);
            }
            library.setBooks(library_books);

            librarys.push_back(library);
        }
    }
    vector<int> seleccionados;
    vector<Library> final;
    while(librarys.size() > 0 && days > 0){
        int pos = 0;
        int val = 0;
        for(int i = 0; i < librarys.size(); i++){
            librarys[i].quitaMierda(seleccionados);
            int aux = librarys[i].maxScoreInXDays(days);
            // Nos quedamos con el mejor
            if(aux > val){
                val = aux;
                pos = i;
            }
        }
        meteMierda(seleccionados,librarys[pos].getBook());

        days = days - librarys[pos].getTimeToScan();
        // Retiramos el libreria del vector
        final.push_back(librarys[pos]);
        librarys.erase(librarys.begin()+pos);
    }
    escribirResultadoEnFichero(final,"hola.txt");

    return 0;
}