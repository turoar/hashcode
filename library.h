#ifndef _LIBRARY_H_
#define _LIBRARY_H_
#include "book.h"
#include <iostream>
#include <vector>

using namespace std;

class Library
{
private:
    int id = 0;

    int timeToScan = 0;

    int scanPerDay = 0;

    vector<Book> books;

public:
    Library()
    {
        this->id = 0;
        this->timeToScan = 0;
        this->scanPerDay = 0;
    }

    Library(int id, int timeToScan, int scanPerDay, vector<Book> books)
    {
        this->id = id;
        this->timeToScan = timeToScan;
        this->scanPerDay = scanPerDay;
        this->books = books;
    }

    void setID(int id){this->id = id;};

    void setTimeToScan(int timeToScan){this->timeToScan = timeToScan;};

    void setScanPerDay(int scanPerDay){this->scanPerDay = scanPerDay;};

    int getID() { return id; }

    int getTimeToScan() { return timeToScan; }

    int getScanPerDay() { return scanPerDay; }

    vector<Book> getBook() { return books; }

    void setBooks(vector<Book> bookies) { books = bookies; }

    void ordenarArrayAscendente()
    {

        Book aux;
        for (int i = 0; i < books.size(); i++)
        {
            for (int x = i + 1; x < books.size(); x++)
            {
                if (books[i].getScore() < books[x].getScore())
                {
                    aux = books[i];
                    books[i] = books[x];
                    books[x] = aux;
                }
            }
        }
        //return books;

        books = books;
    }

    int maxScoreInXDays(int diasTotal)
    {

        int score = 0;
        int z = 0;
        vector<int> puntuacion;

        int dias = diasTotal - timeToScan;

        vector<Book> libros = this->books;
        for (int i = 0; i < dias; i++)
        {
            for (int x = 0; x < scanPerDay; x++)
            {
                score = score + books[z].getScore();
                z++;
            }
        }
        return score;
    }
    int quitaMierda(vector<int> seleccionados){
        
        for(int i = 0 ; i < books.size(); i++){
            for(int j = 0; j < seleccionados.size() ; j++){
                if(books[i].getID() == seleccionados[j]){
                    books.erase(books.begin() + i);
                }
            }
        }
    }
};

#endif