#ifndef _BOOK_H_
#define _BOOK_H_

using namespace std; 

class Book{
    private:

        int id = 0;

        int score = 0;

    public:

        Book(){};

        Book(int id, int score){
            this->id=id;
            this->score=score;
        }

        void setID(int id);

        void setScore(int score);

        int getID(){ return id;}

        int getScore(){ return score;}

};

#endif